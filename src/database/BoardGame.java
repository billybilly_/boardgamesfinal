/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author bombardierib
 */

public class BoardGame {
    protected int id;
    protected String title;
    protected double price;
    protected String players;
    protected int inStock;

    
    //Constructeur
    public BoardGame(int id, String title, double price, String players, int inStock) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.players = players;
        this.inStock = inStock;
    }

    public BoardGame(String title, double price, String players, int inStock) {
        this.title = title;
        this.price = price;
        this.players = players;
        this.inStock = inStock;
    }

    public int getId() {
        return id;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

  

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
 public static ArrayList<BoardGame> allGames() throws ClassNotFoundException, SQLException {   
        Connection connection = Database.getConnection();
        Statement statement = Database.createStatement(connection);
        String query = "SELECT * FROM BoardGame";
        ResultSet resultSet = Database.executeQuery(statement, query);
        
        ArrayList<BoardGame> liste = new ArrayList<>();
        BoardGame game;
        
        while(resultSet.next()){
            int _id = resultSet.getInt("id");
            String _title = resultSet.getString("title");
            double _price = resultSet.getDouble("price");
            String _players = resultSet.getString("players");
            int _inStock = resultSet.getInt("in_stock");
            
            game = new BoardGame(_id, _title, _price, _players, _inStock);
            
            liste.add(game);
        }
        resultSet.close();
        statement.close();
        connection.close();
             
        return liste;       
    }
    
    
    // Permet de récupérer les informations concernants un BoardGame
    public static BoardGame getBoardGame(int id) throws SQLException, ClassNotFoundException {
        BoardGame _game;
        
        Connection connection = Database.getConnection();
        String query = "SELECT * FROM BoardGame WHERE id = (?);";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        
        resultSet.next();
        int _id = resultSet.getInt("id");
        String _title = resultSet.getString("title");
        double _price = resultSet.getDouble("price");
        String _players = resultSet.getString("players");
        int _inStock = resultSet.getInt("in_stock");
       
        _game = new BoardGame(_id, _title, _price, _players, _inStock);
        
        resultSet.close();
        preparedStatement.close();
        connection.close();
        
        return _game;
    }
    
    
    // Permet d'ajouter un BoardGame
    public static void addBoardGame(BoardGame game) throws ClassNotFoundException, SQLException {
        Connection connection = Database.getConnection();
        String query = "INSERT INTO BoardGame (title, players, in_stock, price) VALUES ((?), (?), (?), (?));";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setString(1, game.getTitle());
        preparedStatement.setString(2, game.getPlayers());
        preparedStatement.setInt(3, game.getInStock());
        preparedStatement.setDouble(4, game.getPrice());
        preparedStatement.executeUpdate();
        
        preparedStatement.close();
        connection.close();
    }
    
    // Permet de supprimer un BoardGame
    public static void removeBoardGame(int _id) throws ClassNotFoundException, SQLException {
        Connection connection = Database.getConnection();
        String query = "DELETE FROM BoardGame WHERE id = (?);";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setInt(1, _id);
        preparedStatement.executeUpdate();
        
        preparedStatement.close();
        connection.close();
    }
    
    // Permet de modifier un BoardGame
    public static void updateBoardGame(int _id, BoardGame _newGame) throws SQLException, ClassNotFoundException {
        Connection connection = Database.getConnection();
        String query = "UPDATE BoardGame SET title = (?), players = (?), in_stock = (?), price = (?)  WHERE id = (?);";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setString(1, _newGame.getTitle());
        preparedStatement.setString(2, _newGame.getPlayers());
        preparedStatement.setInt(3, _newGame.getInStock());
        preparedStatement.setDouble(4, _newGame.getPrice());
        preparedStatement.setInt(5, _id);
        preparedStatement.executeUpdate();
        
        preparedStatement.close();
        connection.close();
    }
}

    
    
    
    
    

